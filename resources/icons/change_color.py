import os
import time
import logging
import re

COLOR_DETECTION = "#ED6B21"
COLOR_REPLACE = "#95D1B0"

IGNORE_FILES= ["mode_advanced.svg", "mode_expert.svg", "mode_simple.svg", "mode.svg"]

logging.basicConfig(filename='colorchange.log', encoding='utf-8', level=logging.DEBUG, format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    svg_file_list = []

    for file in os.listdir():
        if ".svg" in file and ".old" not in file:
            if file in IGNORE_FILES:
                print(f'File {file} skipped as it\' as contained in IGNORE_FILES')
                continue
            svg_file_list.append(file)
            print(f'File Found: {file}')
            
    # for i in svg_file_list:
    #     print(f'File found: {i}')


    for svg in svg_file_list:
        filedata = ""
        with open(svg, "r", encoding="UTF-8") as cursvg:
            filedata = cursvg.read()
            
            if COLOR_DETECTION in filedata:
                print ("Found color at " + svg)
            # filedata = filedata.replace(COLOR_DETECTION, COLOR_REPLACE, re.IGNORECASE)
            filedata = re.sub(COLOR_DETECTION, COLOR_REPLACE, filedata, flags=re.IGNORECASE)
            
            if(not os.path.exists(svg + ".old")):
                os.rename(svg, svg + ".old")
            else:
                os.remove(svg)
            
        
        with open(svg, "w", encoding="utf-8") as cursvg:
            cursvg.write(filedata)
            
        
    print("Finished Sucsessfuly!")


# with open("add_copies.svg", "r", encoding="utf-8") as file:
#     string = file.read()
    
            
#     regex = r'#(\d|\w)(\d|\w)(\d|\w)(\d|\w)(\d|\w)(\d|\w)'
#     print(re.findall(regex, string))