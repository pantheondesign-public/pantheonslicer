#!/bin/bash

Build="../build"
BundleName="PantheonSlicer"
AppBundle="pack/$BundleName/$BundleName.app"
Contents="$AppBundle/Contents"
Executable="${Contents}/MacOS/PantheonSlicer"
SIGN_IDENTITY="Developer ID Application: Pantheon Design Ltd. (LXVCBJ7HN9)"

pushd $Build
if [ -f "$BundleName.dmg" ]; then
    echo "Deleting existing image at $BundleName.dmg"
    rm "$BundleName.dmg"
fi

if [ -f "$BundleName.zip" ]; then
    echo "Deleting existing $BundleName.zip"
    rm "$BundleName.zip"
fi

if [ -f "$BundleName.tar" ]; then
    echo "Deleting existing $BundleName.tar"
    rm "$BundleName.tar"
fi

if [ -f "temp.dmg" ]; then
    echo "Deleting existing temp.dmg"
    rm "temp.dmg"
fi


codesign  --options=runtime --force -s "$SIGN_IDENTITY" "$Executable"
ditto -c -k --sequesterRsrc --keepParent "$AppBundle" "$BundleName.zip"
xcrun notarytool submit "$BundleName.zip" --wait --keychain-profile "notarytool-password"
xcrun stapler staple "$AppBundle"
tar -cvf "$BundleName.tar" "pack/$BundleName"
hdiutil create -ov -fs HFS+ -volname "PantheonSlicer" -srcfolder "pack/$BundleName" temp.dmg
hdiutil convert temp.dmg -format UDZO -o "$BundleName.dmg"
xcrun notarytool submit "$BundleName.dmg" --wait --keychain-profile "notarytool-password"
xcrun stapler staple "$BundleName.dmg"
popd